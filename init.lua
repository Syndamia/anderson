print = vim.print
function rawgetrec(table, key, ...)
	if key == nil then
		return table
	end
	return get(rawget(table, key), ...)
end

require("config.lazy")
require("onfiletype")
require("mappings")
require("text")
require("ux")
