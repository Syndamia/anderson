--[=[ Terminal buffers ]=]--

vim.api.nvim_create_autocmd({ "TermOpen", "BufEnter" }, {
	pattern = "term://*",
	command = "setlocal nonu | startinsert",
})
