-- Thanks https://stackoverflow.com/a/70760302/12036073
vim.o.updatetime = 600
vim.diagnostic.config({
	virtual_text = false,
	float = { focus = false },
})
vim.api.nvim_create_autocmd({ "CursorHold", "CursorHoldI" }, { callback = function(ev)
	vim.diagnostic.open_float(nil)
end})

return {
	-- Syntax parsing (for highlighting)
	{
		"nvim-treesitter/nvim-treesitter",
		lazy = false,
		build = ":TSUpdate",
		opts = {
			ensure_installed = "all",
			ignore_install = { "liquidsoap", "tlaplus", "yaml" }, -- Can't compile
			highlight = { enable = true },
			incremental_selection = { enable = true },
			textobjects = { enable = true },
		},
		config = function(_, opts)
			require("nvim-treesitter.configs").setup(opts)
		end,
	},
	-- LSP server utilisation
	{
		"neovim/nvim-lspconfig",
		dependencies = { 'VonHeikemen/lsp-zero.nvim', 'williamboman/mason-lspconfig.nvim' },
		lazy = false,
		opts = { -- :h lspconfig-all
			-- awk_ls = { },
			bashls = { },
			lua_ls = { },
			rust_analyzer = { },
		},
		config = function(_, opts)
			for k,v in pairs(opts) do
				require("lspconfig")[k].setup(v)
			end
		end,
	},
	-- Auto-completion
	{
		'hrsh7th/cmp-nvim-lsp',
		config = true,
		lazy = true
	},
	{
		'hrsh7th/nvim-cmp',
		dependencies = { 'hrsh7th/vim-vsnip' },
		lazy = false,
		opts = {
			sources = { { name = 'nvim_lsp' } },
			snippet = {
				expand = function(args)
				  vim.fn["vsnip#anonymous"](args.body)
				end,
			},
		},
		config = function(_, opts)
			local cmp = require('cmp')
			opts.mapping = cmp.mapping.preset.insert({
			-- 	-- `Enter` key to confirm completion
				['<Tab>'] = cmp.mapping.confirm({select = false}),
			--
			-- 	-- Ctrl+Space to trigger completion menu
				['<C-Space>'] = cmp.mapping.complete(),
			--
			-- 	-- Navigate between snippet placeholder
			-- 	['<C-f>'] = cmp_action.vim_snippet_jump_forward(),
			-- 	['<C-b>'] = cmp_action.vim_snippet_jump_backward(),
			--
			-- 	-- Scroll up and down in the completion documentation
			-- 	['<C-u>'] = cmp.mapping.scroll_docs(-4),
			-- 	['<C-d>'] = cmp.mapping.scroll_docs(4),
			})
			cmp.setup(opts)
		end,
	},
	-- Helper functions
	{
		'VonHeikemen/lsp-zero.nvim',
		branch = 'v3.x',
		lazy = true,
		dependencies = { 'hrsh7th/cmp-nvim-lsp' },
		opts = {
			sign_text = true,
			capabailities = "",
			lsp_attach = function(event)
				local opts = { silent = true, buffer = event.buf }

				vim.keymap.set('n', 'K', '<cmd>lua vim.lsp.buf.hover()<cr>', opts)
				vim.keymap.set('n', 'gd', '<cmd>lua vim.lsp.buf.definition()<cr>', opts)
				vim.keymap.set('n', 'gD', '<cmd>lua vim.lsp.buf.declaration()<cr>', opts)
				vim.keymap.set('n', 'gi', '<cmd>lua vim.lsp.buf.implementation()<cr>', opts)
				vim.keymap.set('n', 'go', '<cmd>lua vim.lsp.buf.type_definition()<cr>', opts)
				vim.keymap.set('n', 'gr', '<cmd>lua vim.lsp.buf.references()<cr>', opts)
				vim.keymap.set('n', 'gs', '<cmd>lua vim.lsp.buf.signature_help()<cr>', opts)
				vim.keymap.set('n', '<F2>', '<cmd>lua vim.lsp.buf.rename()<cr>', opts)
				vim.keymap.set({'n', 'x'}, '<F3>', '<cmd>lua vim.lsp.buf.format({async = true})<cr>', opts)
				vim.keymap.set('n', '<F4>', '<cmd>lua vim.lsp.buf.code_action()<cr>', opts)
			end,
		},
		config = function(_, opts)
			opts.capabilities = require('cmp_nvim_lsp').default_capabilities()
			vim.api.nvim_create_autocmd('LspAttach', {
				desc = 'Lsp Actions',
				callback = opts.lsp_attach,
			})
			require("lsp-zero").extend_lspconfig(opts)
		end,
	},
	-- Download plugins automatically
	{
		'williamboman/mason-lspconfig.nvim',
		dependencies = { 'neovim/nvim-lspconfig' },
		lazy = true,
		opts = {
			automatic_installation = true,
			handlers = {
				function(server_name)
					require('lspconfig')[server_name].setup({})
				end,
			},
		},
	},
	{
		'williamboman/mason.nvim',
		lazy = false,
		opts = {
			PATH = "append",
		},
	},
}
