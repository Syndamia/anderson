return {
	--[=[ Visuals ]=]--
	{
		"morhetz/gruvbox", -- Color theme
		lazy = false,
		priority = 1000,
		init = function()
			vim.o.background = "dark"
			vim.g.gruvbox_contrast_dark = "hard"
			vim.opt.termguicolors = false;
		end,
		config = function(_, opts)
			vim.cmd("colorscheme gruvbox")
		end,
	},
	{
		"https://gitlab.com/Syndamia/gruvbox-highlight.nvim",
		dependencies = { "morhetz/gruvbox" },
		lazy = false,
		priority = 1,
		opts = {
			text = false,
			markup = {
				underline = false,
			},
		},
	},

	--[=[ Quality of life ]=]--
	{
		"preservim/nerdtree", -- Browse directories
		dependencies = {
			"ryanoasis/vim-devicons",
		},
		keys = {{ "<Tab>", ":NERDTreeToggle<CR>" }},
		init = function()
			vim.g.NERDTreeShowHidden = 1
			vim.g.NERDTreeWinPos     = "right"
			vim.g.NERDTreeIgnore     = { "\\.swp$", "\\~$" } -- Ignore file, ending with .swp and ~
		end,
	},
	{
		"nvim-tree/nvim-web-devicons", -- NERDFont icons
	},
	{
		"numToStr/Comment.nvim", -- Toggle comments
		opts = {},
	},
	{
		"mbbill/undotree", -- Easily interact with undo history
		keys = {{ "<F5>", ":UndotreeToggle<CR>" }},
		init = function()
			vim.g.undotree_WindowLayout       = 2
			vim.g.undotree_ShortIndicators    = 1 -- e.g. using 'd' instead of 'days' to save some space.
			vim.g.undotree_SetFocusWhenToggle = 1 -- if set, let undotree window get focus after being opened, otherwise focus will stay in current window.
			vim.g.undotree_TreeNodeShape      = '*'
			vim.g.undotree_DiffCommand        = "diff"
		end,
	},
	{
		"godlygeek/tabular", -- Line up text to a given character
	},
	{
		"tpope/vim-eunuch", -- UNIX commands in vim
	},
	{
		"kylechui/nvim-surround", -- Surround text
		version = "*",
		event = "VeryLazy",
		dependencies = {
			"nvim-treesitter/nvim-treesitter", "nvim-treesitter/nvim-treesitter-textobjects"
		},
		opts = {},
	},
	{
		"windwp/nvim-ts-autotag", -- Auto-add and edit HTML tags
	},
	{
		"ntpeters/vim-better-whitespace", -- Highlight in red trailing whitespaces
	},

	--[=[ Development-specific ]=]--
	{
		"zivyangll/git-blame.vim", -- Show last commit on line
		keys = {{ "<F8>", ":<C-u>call gitblame#echo()<CR>" }},
	},
	{
		"wakatime/vim-wakatime", -- WakaTime tracking
		lazy = false,
	},
	{
		"nvim-neorg/neorg",
		lazy = true,
		ft = "norg",
		version = "8.9.0", -- Last version to support versions <nvim-0.9
		config = true,
		opts = {
			load = {
				["core.defaults"] = {},
				["core.concealer"] = { config = { -- Display markup as icons
					icon_preset = "varied",
				}},
				["core.export"] = {},
				["core.export.markdown"] = {},
				-- ["core.presenter"] = {}, -- Requires zen mode
				-- ["core.text-objects"] = {}, -- Requires >=nvim-0.10
				["core.esupports.metagen"] = {}, -- Autoplace metadata
			},
		},
	},
	{ "noir-lang/noir-nvim", lazy = false },
}
