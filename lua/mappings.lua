--[=[ Helpers ]=]--

keys = {}
setmetatable(keys,
{
	__index = function (table, index)
		local mod, key = index:match("^(.*)_"), index:match("[^_]*$")
		if mod == nil then
			if #key > 1 then key = "<" .. key .. ">" end
			return key
		end
		mod = mod:upper()

		if     mod == "SHIFT"   then key = "<S-" .. key .. ">"
		elseif mod == "CONTROL" then key = "<C-" .. key .. ">"
		elseif mod == "ALT"     then key = "<M-" .. key .. ">"
		elseif mod == "SUPER"   then key = "<D-" .. key .. ">"
		elseif mod ~= nil       then error("Unknown key modifier \"" .. mod .. "\"!")
		end

		return key
	end
})
map = { keys = { lhs = {}, rhs = "" } }
setmetatable(map,
{
	__index = function (table, index)
		table.keys.lhs = {}
		table.keys.rhs = ""
		for i = 1, index:len() do
			table.keys.lhs[i] = index:sub(i, i)
		end
		return table.keys
	end
})
setmetatable(map.keys,
{
	__index = function (table, index)
		table.rhs = table.rhs .. keys[index]
		return table
	end,
	__call = function (table, ...)
		return vim.keymap.set(table.lhs, table.rhs, ...)
	end
})

local function icommand(command)
	return keys.Control_o .. command .. keys.Return
end

--[=[ Mappings ]=]--

-- Quality of life
map.t["Esc><Esc"]("<C-\\><C-N>")

-- Tab navigation
map.n.Alt_p("gt")
map.n.Alt_o("gT")

map.i.Alt_p(keys.Control_o .. "gt")
map.i.Alt_o(keys.Control_o .. "gT")

map.t.Alt_p("<C-\\><C-N>gt")
map.t.Alt_o("<C-\\><C-N>gT")

-- Split navigation
local function windownav(letter) return keys.Control_w .. letter end
map.n.Control_l(windownav("l"))
map.n.Control_h(windownav("h"))
map.n.Control_j(windownav("j"))
map.n.Control_k(windownav("k"))
map.t.Control_l("<C-\\><C-o>" .. windownav("l") .. "<Esc>")
map.t.Control_h("<C-\\><C-o>" .. windownav("h") .. "<Esc>")
map.t.Control_j("<C-\\><C-o>" .. windownav("j") .. "<Esc>")
map.t.Control_k("<C-\\><C-o>" .. windownav("k") .. "<Esc>")

-- Ctrl-S, Ctrl-A, Ctrl-C, Ctrol-X
map.i.Control_s(icommand(":w"))
map.i.Control_a(keys.Escape .. "ggVG")

map.n.Control_s(":w" .. keys.Enter)
map.n.Control_a("ggVG")

map.x.Control_c('"+y')
map.x.Control_x('"+d')

-- Undo/redo
map.i.Control_r(keys.Control_o .. keys.Control_r)
map.i.Control_u(keys.Control_o .. "u")

-- Backspace
map.ic.Control_h(keys.Control_w)
map.ic.Control_BS(keys.Control_w)

-- Move lines
map.n.Alt_j(":m+1" .. keys.Enter)
map.n.Alt_k(":m-2" .. keys.Enter)
map.x.Alt_j(":m '>+1<CR>gv=gv")
map.x.Alt_k(":m '<-2<CR>gv=gv")

-- Special
map.n["|"](":tabnew | ter lazygit<CR>", { silent = true })
map.n.F6(function()
	vim.cmd("set spell!")
end)

--[=[ Abbreviations ]=]--
vim.cmd("cnoreabbrev x wa \\| qa!")

--[=[ Language mappings ]=]--
vim.opt.langmap = {
	-- Lowercase mappings
	"аa", "бb", "вw", "гg", "дd", "еe", "жv", "зz", "иi", "йj", "кk", "лl", "мm", "нn",
	"оo", "пp", "рr", "сs", "тt", "уu", "фf", "хh", "цc", "ч`", "ш[", "щ]", "ъy", "ьx", "ю\\", "яq",

	--, Uppercase, mappings
	"АA", "БB", "ВW", "ГG", "ДD", "ЕE", "ЖV", "ЗZ", "ИI", "ЙJ", "КK", "ЛL", "МM", "НN",
	"ОO", "ПP", "РR", "СS", "ТT", "УU", "ФF", "ХH", "ЦC", "Ч~", "Ш[", "Щ]", "ЪY", "ЬX", "Ю|", "ЯQ",
}
