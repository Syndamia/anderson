--[=[ Search ]=]--

vim.o.hlsearch = false -- No highlighting when searching

--[=[ Cursor and mouse ]=]--

vim.o.scrolloff = 5    -- Lines above and below cursor
vim.o.showmatch = true -- Jump to opening brace when having typed closing one

vim.o.cursorline = true
vim.api.nvim_create_autocmd({ "InsertLeave", "BufEnter" }, {
	command = "set cul"
})
vim.api.nvim_create_autocmd({ "InsertEnter", "BufLeave" }, {
	command = "set nocul"
})

vim.o.mouse = "a"

--[=[ Windows ]=]--

vim.o.splitbelow = true
vim.o.splitright = true

vim.o.number = true
vim.o.signcolumn = "number"

vim.o.showmode = false
vim.o.timeoutlen = 350

--[=[ History ]=]--

vim.o.undofile = true
vim.o.undodir  = vim.fn.expand("~/nvim/undo")
