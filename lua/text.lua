vim.cmd.highlight({ "Pmenu", "ctermfg=223", "ctermbg=238" })

--[=[ Treesitter ]=]--

vim.cmd.highlight({ "link", "@variable", "GruvboxFg1" })
vim.cmd.highlight({ "link", "@operator", "GruvboxOrange" })

--[=[ Characters ]=]--

vim.o.list = true
vim.opt.listchars = { tab = "│ ", extends = ">", precedes = "<" }

vim.o.tabstop    = 4 -- Show tab as 4 spaces
vim.o.shiftwidth = 4 -- Indent with 4 spaces

--[=[ Folding ]=]--
vim.o.foldlevel = 99
vim.cmd.highlight({ "Folded", "ctermfg=NONE" })

--[=[ ToHTML ]=]--
vim.g.html_dynamic_folds = true
